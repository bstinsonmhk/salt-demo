Firefox:
  27.0:
    installer: 'http://ftp.mozilla.org/pub/mozilla.org/firefox/releases/27.0/win32/en-US/Firefox%20Setup%2027.0.exe'
    full_name: 'Mozilla Firefox 27.0 (x86 en-US)'
    reboot: False
    install_flags: ' /s '
    uninstaller: 'C:\Program Files (x86)\Mozilla Firefox\uninstall\helper.exe'
    uninstall_flags: ' /S'
