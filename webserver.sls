nginx:
  pkg:
    - latest
  service:
    - running
    - enable: True
    - require:
      - pkg: nginx
    - watch:
      - file: /etc/nginx/conf.d/*

/etc/nginx/conf.d/archipel.conf:
  file.managed:
    - source: salt://files/archipel.conf
    - user: root
    - group: root
    - mode: 0644

/var/www/html:
  file.directory:
    - makedirs: True

/var/www/archipel-gui-beta6:
  file.recurse:
    - source: salt://files/archipel-gui-beta6
    - require:
      - file: /var/www/html

/var/www/html/archipel:
  file.symlink:
    - target: /var/www/archipel-gui-beta6
